# Лабораторная работа №4

## **Docker: Мультистейджинг, сборка и запуск**

Цель лабораторной: освоить подход создания легковесного образа путем сборки приложения на одном образе, и запуска на другом. Рекомендуется использовать любое опенсорс приложение требующее SDK для сборки.

1. Образ должен быть легковесным
2. Сборка приложения должна происходить в первом образе
3. Запуск приложения выполняется в новом образе, через копирование артефакта
4. Вся конфигурация выполняется через переменные окружения
5. Всё оформлено в одном Dockerfile
6. Создать файл docker-compose для старта и сборки
7. Зависимости из первого образа переходят во второй как volume
8. Образ должен быть на базе scratch

Файлы: Dockerfile, docker-compose.yml.
```
docker compose up --build
```
# Лабораторная работа №5

## **Docker: Мультистейджинг, различные уровни зависимостей**

Цель лабораторной: освоить методы создания образов с разными уровнями зависимостей

1. Для системных зависимостей приложения создаем образ name:system
2. Для зависимостей сборки создаем образ name:build
3. Для приложения создаем образ name:app
4. Вся конфигурация выполняется через переменные окружения
5. Освоить параметр --cache-from
6. Образ должен быть на базе scratch


Файлы: Dockerfile.system, Dockerfile.build, Dockerfile.app.

Собрать Docker-образы:

   ```console
   $ docker build -t name:system -f Dockerfile.system .
   $ docker build -t name:build -f Dockerfile.build .
   $ docker build -t name:app -f Dockerfile.app .
   ```

Запустить контейнер из созданного образа:

   ```console
   $ docker run -p 8080:8080 \
       -e DB_HOST=<database_host> \
       -e DB_PORT=<database_port> \
       -e DB_NAME=<database_name> \
       -e DB_USERNAME=<database_username> \
       -e DB_PASSWORD=<database_password> \
       -e SPRING_PROFILES_ACTIVE=<profiles> \
       name:app
   ```

   где `<database_host>`, `<database_port>`, `<database_name>`, `<database_username>`, `<database_password>` и `<profiles>` - значения соответствующих переменных окружения.

Для повышения производительности сборки образов можно использовать параметр --cache-from:

   ```console
   $ docker build --cache-from name:system --target system -t name:system -f Dockerfile.system .
   $ docker build --cache-from name:build --target build -t name:build -f Dockerfile.build .
   $ docker build --cache-from name:system --cache-from name:build --target app -t name:app -f Dockerfile.app .
   ```
